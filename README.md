# probator-auditor-vpc-flowlogs

Please open issues in the [Probator](https://gitlab.com/probator/probator/issues/new?labels=auditor-vpc-flow-logs) repository

## Description

Enabling this plugin will ensure that all your VPC's have VPC Flow Logs turned on. You can configure the logs to either be delivered to an
S3 bucket you provide, or to auto-generated CloudWatch Logs groups.

If using CloudWatch Logs, a new log group will be created in the same region as the VPC, with the VPC Id as the log group name. S3 buckets
will not be created automatically

## Configuration Options

| Option name       | Default Value     | Type      | Description                               |
|-------------------|-------------------|-----------|-------------------------------------------|
| enabled           | False             | bool      | Enable the VPC Flow Logs auditor          |
| interval          | 60                | int       | Run frequency in minutes                  |
| role\_name        | `VpcFlowLogsRole` | string    | Name of IAM Role used for VPC Flow Logs   |
| delivery\_method  | `s3`              | string    | Log delivery method                       |
| traffic\_type     | `ALL`             | string    | Traffic type to log                       |
| bucket\_name      | *None*            | string    | Bucket name to deliver logs to            |

This project is based on the work for [Cloud Inquisitor](https://github.com/RiotGames/cloud-inquisitor) by Riot Games
